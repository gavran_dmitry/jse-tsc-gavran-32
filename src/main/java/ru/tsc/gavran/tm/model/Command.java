package ru.tsc.gavran.tm.model;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
public class Command {

    @NotNull
    private String name;

    @Nullable
    private String argument;

    @NotNull
    private String description;

    public Command(@NotNull String name, @Nullable String argument, @NotNull String description) {
        this.name = name;
        this.argument = argument;
        this.description = description;
    }

    @Override
    public String toString() {
        String result = "";
        if (name != null && !name.isEmpty()) result += name + " ";
        if (argument != null && !argument.isEmpty()) result += "(" + argument + ") ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

}