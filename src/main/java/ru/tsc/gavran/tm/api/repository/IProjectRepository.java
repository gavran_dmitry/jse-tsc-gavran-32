package ru.tsc.gavran.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.enumerated.Status;
import ru.tsc.gavran.tm.model.Project;

public interface IProjectRepository extends IOwnerRepository<Project> {

    @NotNull
    Project findByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project removeByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project startById(@NotNull String userId, @NotNull String id);

    @Nullable
    Project startByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project startByIndex(@NotNull String userId, @NotNull int index);

    @Nullable
    Project finishById(@NotNull String userId, @NotNull String id);

    @Nullable
    Project finishByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project finishByIndex(@NotNull String userId, @NotNull int index);

    @Nullable
    Project changeStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status);

    @Nullable
    Project changeStatusByName(@NotNull String userId, @NotNull String name, @NotNull Status status);

    @Nullable
    Project changeStatusByIndex(@NotNull String userId, @NotNull int index, @NotNull Status status);


}